# Satifaction Slider jQuery plugin

Réalisation d'un slider pour capturer le niveau de satisfaction des utilisateurs.

Me première idée aurait été de simplement styliser l'élément input[type="range"].
Exemple : http://danielstern.ca/range.css/#/

Mais le CSS ne permet pas d'obtenir l'effet de dégrader vers la droite et vers la gauche.
L'apparaition du smilez au-dessus du curseur devant également faire intervenir du JS
je vais plutôt développer un petit plugin jQuery pour transformer les éléments de type input[type=range] afin d'intégrer ces suptilités.

Le choix d'un plugin jQuery garanti également la facilité de mise en oeuvre et de réutilisation dans de futurs projets.

A noter qu'il existe pas mal de plugin de type range picker, mais je n'ai pas trouver de version avec une position centrale neutre et les différents effets graphiques souhaités.

Pour voir une démo et visualiser la documentation il suffit d'ouvrir /dist/index.html du package.