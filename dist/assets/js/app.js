$(() => {
    //init du plugin jquery satifaction slider
    $('input[name="satisfaction-level"]').satisfactionSlider({
        //ici les options du plugin
        onDragStart : () => { $('.modal-popup-header').animate({opacity: 0.2},700); },
        onDragEnd : () => { $('.modal-popup-header').animate({opacity:1}, 700); }
    });
});