const gulp = require('gulp');
// Include plugins
const sass = require('gulp-sass');
const rename = require('gulp-rename');
const csso = require('gulp-csso');
const autoprefixer = require('gulp-autoprefixer');
const minify = require('gulp-minify');

// paths
var source = './src';
var destination = './dist';

gulp.task('css', function () {
    return gulp.src(source + '/assets/scss/**/*.scss')
    /* ici les plugins Gulp à exécuter */
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(csso())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(destination + '/assets/css'));
});

gulp.task('js', function () {
    return gulp.src(source + '/assets/js/**/*.js')
    /* ici les plugins Gulp à exécuter */
        .pipe(minify())
        .pipe(gulp.dest(destination + '/assets/js'));
});