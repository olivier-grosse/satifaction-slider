// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

;(function($) {
    var _DEBUG_LEVEL = 0; // from 0 = no debug message to 10
    // here we go!
    $.satisfactionSlider = function(element, options) {

        // plugin's default options
        // this is private property and is  accessible only from inside the plugin
        var defaults = {
            hide_original_input: true,
            css_prefix: 'satisfaction-slider-',
            onDragStart: function() {},
            onDragEnd: function() {},
            onDrag: function() {}
        };

        // to avoid confusions, use "plugin" to reference the current instance of the object
        var plugin = this;

        // this will hold the merged default, and user-provided options
        // plugin's properties will be available through this object like:
        // plugin.settings.propertyName from inside the plugin or
        // element.data('satisfactionSlider').settings.propertyName from outside the plugin, where "element" is the
        // element the plugin is attached to;
        plugin.settings = {};

        var $element = $(element),  // reference to the jQuery version of DOM element the plugin is attached to
            element = element,        // reference to the actual DOM element
            $progress, $handle, $track, $component, $handle_live_icon, //component part jquery instance ref
            track_offset_top, track_offset_left, track_width, handle_width, handle_height, //positions and dimensions
            handle_min_left, handle_max_left, //value calculate only on dimension init
            value_range, value,full_usable_with, relative_position //last values calculated
        ;

        // the "constructor" method that gets called when the object is created
        plugin.init = function() {

            // the plugin's final properties are the merged default and user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

            value_range = parseInt($element.attr('max')) - parseInt($element.attr('min'));


            _init_dom();
            _init_position();
            _init_handlers();


        };

        // public methods
        // these methods can be called like:
        // plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
        // element.data('satisfactionSlider').publicMethod(arg1, arg2, ... argn) from outside the plugin, where "element"
        // is the element the plugin is attached to;

        // a public method. for demonstration purposes only - remove it!
        plugin.foo_public_method = function() {

            // code goes here

        };

        /**
         * inject html element for plugin UX
         * @private
         */


        var null_pixel = new Image();



        var _init_dom = function() {

            if(plugin.settings.hide_original_input) {
                $element.addClass('satisfaction-slider-input');
            }

            $progress = $('<div class="' + plugin.settings.css_prefix + 'progress">');
            $handle_live_icon = $('<img class="' + plugin.settings.css_prefix + 'handle-live-icon">');
            $handle = $('<div class="' + plugin.settings.css_prefix + 'handle" draggable="true" >').append($handle_live_icon);
            $track = $('<div class="' + plugin.settings.css_prefix + 'track">')
                .append($progress)
                .append($handle)

            $component = $('<div class="' + plugin.settings.css_prefix + 'component">').append($track);

            $element.before($component);

            null_pixel.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
        };

        var _init_handlers = function() {
            $handle.draggable({
                axis : 'x',
                left_range : [track_offset_left, track_offset_left + track_width - handle_width],
                scroll:false,
                onDrag : function() {
                    let offset =$handle.offset();
                    _update_value(offset);
                    plugin.settings.onDrag();
                },
                onStart : () => {
                    $handle.addClass('drag');
                    plugin.settings.onDragStart();
                },
                onStop : () => {
                    $handle.removeClass('drag');
                    plugin.settings.onDragEnd();
                },
            });

            $(window).on('resize', e => {
                if(_DEBUG_LEVEL > 8) {
                    console.log('resize event');
                }
                _init_position();

                _update_value($handle.offset());
                $handle.data('draggable').options.left_range = [track_offset_left, track_offset_left + track_width - handle_width];
            });
        };

        /**
         convert handle position in value
         */
        var _update_value = function(offset) {

            full_usable_with = track_width - handle_width; //need to remove half with of handle left and same right.
            relative_position = offset.left - track_offset_left;

            value = Math.round(value_range * relative_position / full_usable_with);

            if(_DEBUG_LEVEL>0) {
                console.log('the value ' + value + '');
            }

            $element.val(value);
            _redraw_progress_bar(relative_position, full_usable_with);
            _redraw_live_icon(value);
        };

        var _redraw_live_icon = function() {
            let $option = $('#' + $element.attr('list') + '>option[value="'+value+'"]');
            let icon_path = $option.data('icon');
            let label = $option.attr('label');

            if(icon_path != undefined)
            {
                if(_DEBUG_LEVEL > 6) {
                    console.log(icon_path);
                }
                $handle_live_icon.attr('src', icon_path);
            }
            if(label != undefined)
            {
                if(_DEBUG_LEVEL > 6) {
                    console.log(label);
                }
                $handle_live_icon.attr('alt', label).attr('title', label);
            }
        };

        var _redraw_progress_bar = function() {
            if(relative_position < full_usable_with/2)
            {
                $progress.css({
                    'width': (full_usable_with/2 - relative_position) + 'px',
                    'margin-left': relative_position + handle_width/2 + 'px',
                    'transform': 'rotate(0)'
                });
            }
            else
            {
                $progress.css({
                    'width': (relative_position - full_usable_with/2) + 'px',
                    'margin-left': (full_usable_with/2 + handle_width/2) + 'px',
                    'transform': 'rotate(180deg)'
                });
            }


        };

        var _init_position = function() {
            let offset = $track.offset();
            //save rendered positions and dimensions of track and handle parts
            track_offset_left = offset.left;
            track_offset_top = offset.top;
            track_width = $track.width();
            handle_width = $handle.width();
            handle_height = $handle.height();
            handle_min_left = track_offset_left - handle_width/2;
            handle_max_left = track_offset_left + track_width - handle_width/2;

            if(_DEBUG_LEVEL > 5)
            {
                console.log('Track position : { '+track_offset_left+';'+track_offset_top+' } and width : ' + track_width + 'px');
            }

            _init_handle_position();
        };

        var _init_handle_position = function() {
            $handle.css({
                top:(track_offset_top - handle_height/2)  + 'px',
                left:(track_offset_left + track_width/2 - handle_width/2)  + 'px'
            });
        };


            // fire up the plugin!
        // call the "constructor" method
        plugin.init();

    };

    // add the plugin to the jQuery.fn object
    $.fn.satisfactionSlider = function(options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('satisfactionSlider')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.satisfactionSlider(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('satisfactionSlider').publicMethod(arg1, arg2, ... argn) or
                // element.data('satisfactionSlider').settings.propertyName
                $(this).data('satisfactionSlider', plugin);

            }

        });

    };

})(jQuery);
