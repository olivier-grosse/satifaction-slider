/**
 * https://github.com/gardiner/draganddrop
 * licence MIT
 *
 * modified by Grosse Olivier
 */
(function($) {
    "use strict";
    function Draggable(el, options) {
        var self = this,
            defaults = {
                //options
                axis: 'x',
                left_range : [],
                handle: false,
                delegate: false,
                revert: false,
                placeholder: false,
                droptarget: false,
                container: false,
                scroll: false,
                //callbacks
                onStart: null,
                onDrag: null,
                onStop: null
            };

        self.$draggable = $(el).data('draggable', self);
        self.options = $.extend({}, defaults, options);

        self.init();
    }

    Draggable.prototype.init = function() {
        var self = this,
            $clone,
            origin;

        self.$draggable
            .addClass('draggable')
            .on('destroy.draggable', function() {
                self.destroy();
            });

        function check_droptarget(pos) {
            var $over;

            $('.hovering').removeClass('hovering');

            $clone.hide();
            $over = $(document.elementFromPoint(pos.clientX, pos.clientY)).closest(self.options.droptarget);
            $clone.show();

            if ($over.length) {
                $over.addClass('hovering');
                return $over;
            }
        }

        self.$draggable.dragaware($.extend({}, self.options, {
            /**
             * drag start - create clone, keep drag start origin.
             */
            dragstart: function(evt) {
                var $this = $(this);
                if (self.options.placeholder || self.options.revert) {
                    $clone = $this.clone()
                        .removeAttr('id')
                        .addClass('draggable_clone')
                        .css({position: 'absolute'})
                        .appendTo(self.options.container || $this.parent())
                        .offset($this.offset());
                    if (!self.options.placeholder) {
                        $(this).invisible();
                    }
                } else {
                    $clone = $this;
                }

                origin = new PositionHelper($clone.offset());

                if(self.options.onStart != undefined) {
                    self.options.onStart();
                }
            },

            /**
             * drag - reposition clone.
             */
            drag: function(evt, pos) {
                var $this = $(this);

                var new_offset = origin.absolutize(pos);

                if(self.options.axis == 'x')
                {
                    new_offset.top='10px';
                }

                //in order to constrain mouvement
                if(self.options.left_range.length == 2)
                {
                    new_offset.left = Math.max(self.options.left_range[0], Math.min(new_offset.left, self.options.left_range[1]));
                }

                $clone.offset(new_offset);

                if(self.options.onDrag != undefined) {
                    self.options.onDrag();
                }
            },

            /**
             * drag stop - clean up.
             */
            dragstop: function(evt, pos) {
                var $this = $(this),
                    $droptarget = check_droptarget(pos);

                if (self.options.revert || self.options.placeholder) {
                    $this.visible();
                    if (!self.options.revert) {
                        $this.offset(origin.absolutize(pos));
                    }
                    $clone.remove();
                }

                $clone = null;

                if (self.options.update) {
                    self.options.update.call($this, evt, self);
                }

                if(self.options.onStop != undefined) {
                    self.options.onStop();
                }

                if ($droptarget) {
                    if (self.options.drop) {
                        self.options.drop.call($this, evt, $droptarget[0]);
                    }
                    $droptarget.trigger('drop', [$this]);
                    $droptarget.removeClass('hovering');
                }
            }
        }));
    };

    Draggable.prototype.destroy = function() {
        var self = this;

        self.$draggable
            .dragaware('destroy')
            .removeClass('draggable')
            .off('.draggable');
    };



    function Dragaware(el, options) {
        var $dragaware = $(el),
            $reference = null,
            origin = null,
            lastpos = null,
            defaults = {
                //options
                handle: null,
                delegate: null,
                scroll: false,
                scrollspeed: 15,
                scrolltimeout: 50,
                //callbacks
                dragstart: null,
                drag: null,
                dragstop: null
            },
            scrolltimeout;

        options = $.extend({}, defaults, options);

        /**
         * Returns the event position
         * dX, dY relative to drag start
         * pageX, pageY relative to document
         * clientX, clientY relative to browser window
         */
        function evtpos(evt) {
            evt = window.hasOwnProperty('event') ? window.event : evt;
            //extract touch event if present
            if (evt.type.substr(0, 5) === 'touch') {
                evt = evt.hasOwnProperty('originalEvent') ? evt.originalEvent : evt;
                evt = evt.touches[0];
            }

            return {
                pageX: evt.pageX,
                pageY: evt.pageY,
                clientX: evt.clientX,
                clientY: evt.clientY,
                dX: origin ? evt.pageX - origin.pageX : 0,
                dY: origin ? evt.pageY - origin.pageY : 0
            };
        }

        function autoscroll(pos) {
            //TODO: allow window scrolling
            //TODO: handle nested scroll containers
            var sp = $dragaware.scrollParent(),
                mouse = {x: pos.pageX, y: pos.pageY},
                offset = sp.offset(),
                scrollLeft = sp.scrollLeft(),
                scrollTop = sp.scrollTop(),
                width = sp.width(),
                height = sp.height();

            window.clearTimeout(scrolltimeout);

            if (scrollLeft > 0 && mouse.x < offset.left) {
                sp.scrollLeft(scrollLeft - options.scrollspeed);
            } else if (scrollLeft < sp.prop('scrollWidth') - width && mouse.x > offset.left + width) {
                sp.scrollLeft(scrollLeft + options.scrollspeed);
            } else if (scrollTop > 0 && mouse.y < offset.top) {
                sp.scrollTop(scrollTop - options.scrollspeed);
            } else if (scrollTop < sp.prop('scrollHeight') - height && mouse.y > offset.top + height) {
                sp.scrollTop(scrollTop + options.scrollspeed);
            } else {
                return; //so we don't set the next timeout
            }

            scrolltimeout = window.setTimeout(function() { autoscroll(pos); }, options.scrolltimeout);
        }

        function start(evt) {
            var $target = $(evt.target);

            $reference = options.delegate ? $target.closest(options.delegate) : $dragaware;

            if ($target.closest(options.handle || '*').length && (evt.type == 'touchstart' || evt.button == 0)) {
                origin = lastpos = evtpos(evt);
                if (options.dragstart) {
                    options.dragstart.call($reference, evt, lastpos);
                }

                $reference.addClass('dragging');
                $reference.trigger('dragstart');

                //late binding of event listeners
                $(document)
                    .on('touchend.dragaware mouseup.dragaware click.dragaware', end)
                    .on('touchmove.dragaware mousemove.dragaware', move);
                return false
            }
        }

        function move(evt) {
            lastpos = evtpos(evt);

            if (options.scroll) {
                autoscroll(lastpos);
            }

            $reference.trigger('dragging');

            if (options.drag) {
                options.drag.call($reference, evt, lastpos);
                return false;
            }
        }

        function end(evt) {
            window.clearTimeout(scrolltimeout);

            if (options.dragstop) {
                options.dragstop.call($reference, evt, lastpos);
            }

            $reference.removeClass('dragging');
            $reference.trigger('dragstop');

            origin = false;
            lastpos = false;
            $reference = false;

            //unbinding of event listeners
            $(document)
                .off('.dragaware');

            return false;
        }

        $dragaware
            .addClass('dragaware')
            .on('touchstart.dragaware mousedown.dragaware', options.delegate, start);

        $dragaware.on('destroy.dragaware', function() {
            $dragaware
                .removeClass('dragaware')
                .off('.dragaware');
        });
    }




    function PositionHelper(origin) {
        this.origin = origin;
    }
    PositionHelper.prototype.absolutize = function(pos) {
        if (!pos) {
            return this.origin;
        }
        return {top: this.origin.top + pos.dY, left: this.origin.left + pos.dX};
    };




// Plugin registration.

    /**
     * Draggable plugin.
     */
    $.fn.draggable = function(options) {
        if (options === 'destroy') {
            this.trigger('destroy.draggable');
        } else {
            this.not('.draggable').each(function(ix, el) {
                new Draggable(el, options);
            });
        }
        return this;
    };



    /**
     * Dragaware plugin.
     */
    $.fn.dragaware = function(options) {
        if (options === 'destroy') {
            this.trigger('destroy.dragaware');
        } else {
            this.not('.dragaware').each(function(ix, el) {
                new Dragaware(el, options);
            });
        }
        return this;
    };


    /**
     * Disables mouse selection.
     */
    $.fn.unselectable = function(command) {
        function disable() {
            return false;
        }

        if (command == 'destroy') {
            return this
                .removeClass('unselectable')
                .removeAttr('unselectable')
                .off('selectstart.unselectable');
        } else {
            return this
                .addClass('unselectable')
                .attr('unselectable','on')
                .on('selectstart.unselectable', disable);
        }
    };


    $.fn.invisible = function() {
        return this.css({visibility: 'hidden'});
    };


    $.fn.visible = function() {
        return this.css({visibility: 'visible'});
    };


    $.fn.scrollParent = function() {
        return this.parents().addBack().filter(function() {
            var p = $(this);
            return (/(scroll|auto)/).test(p.css("overflow-x") + p.css("overflow-y") + p.css("overflow"));
        });
    };

    $.fn.nestingDepth = function(selector) {
        var parent = this.parent().closest(selector || '*');
        if (parent.length) {
            return parent.nestingDepth(selector) + 1;
        } else {
            return 0;
        }
    };


}(jQuery));